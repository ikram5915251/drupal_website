output "id" {
  value = aws_vpc.main.id
}
output "public_subnet_ids" {
  value = keys({
    for subnet in aws_subnet.public :
    subnet.id => subnet.cidr_block
  })
}
