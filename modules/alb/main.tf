resource "aws_security_group" "lb_sg" {
  name        = "alb-sg"
  description = "Security group for the load balancer"
  vpc_id      = var.vpc_id

  ingress {
    description = "Allow http traffic"
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_lb" "alb" {
  name               = "drupal-fargate-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_sg.id]
  subnets            = [for subnet in keys(var.subnets) : subnet]
}

resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.df_tg.arn
  }
}

resource "aws_lb_target_group" "df_tg" {
  name        = "df-lb-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc_id
  health_check {
    enabled  = true
    path     = "/health"
    interval = 60
    matcher  = "200"
  }
}
#We want the load balancer (lb) to be public facing so we set the internal argument to false. #
#Amazon provides several different types of load balancers, but we want an application load balancer (ALB) so that's what we give as the load_balancer_type argument.#
#The target group is a collection of resources that should receive traffic from the load balancer. There are several ways that the lb can determine which instances belong to this group. #
#In our case we want IP based filtering and for that we must provide the VPC so it knows the IP range to look for. Our nginx container is listening on port 80 to that's the value of the port argument of the module.#
#Health checks are required so that the lb knows which targets are in a state where they can receive requests. I have a route set up in my nginx configuration that lives on the /health route, so we use that value.#
# The final part of this module is the listener. This tells the load balancer to listen on a certain port and then perform an action when traffic is received. We want requests from the lb's port 80 to be forwarded to our ECS service's port 80 so we configure the "default action" to do that.#