resource "aws_security_group" "efs_sg" {
  name        = "efs-sg"
  description = "Security Group for EFS"
  tags        = var.tags
  vpc_id      = var.vpc_id

  ingress {
    description     = "Allow NFS traffic"
    from_port       = "2049"
    to_port         = "2049"
    protocol        = "tcp"
    security_groups = [var.ecs_sg]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_efs_file_system" "files" {
  encrypted = true
  tags      = var.tags
  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }
}

resource "aws_efs_mount_target" "name" {
  for_each        = {for idx, id in var.subnet_ids: id => id}
  file_system_id  = aws_efs_file_system.files.id
  security_groups = [aws_security_group.efs_sg.id]
  subnet_id       = each.value
}

resource "aws_efs_access_point" "files" {
  file_system_id = aws_efs_file_system.files.id
  posix_user {
    uid = 82
    gid = 82
  }
  root_directory {
    creation_info {
      owner_uid   = 82
      owner_gid   = 82
      permissions = 755
    }
    path = "/files"
  }

}
# Since ou file needs to be accessible to all instances of our application , we work woth EFS #
#Similar to the RDS security group, this restricts network traffic to only the NFS port coming from the ECS service#
#Mount target : We create mount target so that the file system can be accesses on each of teh subnets and is locked down via a securoy group def #
# Access points : they allow you to create specific entry points (chroot) on your EFS, essentially locking down access to only the directory that you specify. I'm using an alpine linux based image which has the www-data user's id as 82, so that's the uid and gid that I'm using for the posix_user and owner values of the access point.#
#  By default all files in the EFS are accessible to anyone who mounts the file system.