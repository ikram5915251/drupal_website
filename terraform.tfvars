##########################################
# Variables to pass to the build process #
##########################################

account_id = "668991031085"
#namespace  = "spinspire"
region     = "us-east-1"
profile    = "cloud-user"

tags = {
  "application" = "drupal-fargate"
  "managed-by"  = "terraform"
}